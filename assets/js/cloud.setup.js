/**
 * cloud.setup.js
 *
 * Configuration for this Sails app's generated browser SDK ("Cloud").
 *
 * Above all, the purpose of this file is to provide endpoint definitions,
 * each of which corresponds with one particular route+action on the server.
 *
 * > This file was automatically generated.
 * > (To regenerate, run `sails run rebuild-cloud-sdk`)
 */

Cloud.setup({

  /* eslint-disable */
  methods: {"confirmEmail":{"verb":"GET","url":"/email/confirm","args":["token"]},"addUser":{"verb":"GET","url":"/1at:token","args":["token"]},"list":{"verb":"GET","url":"/admin/attendance","args":[]},"view":{"verb":"GET","url":"/admin/attendance/:id","args":["id","page"]},"addUserToAttendance":{"verb":"GET","url":"/admin/attendance/:id/add/:userId","args":["id","userId"]},"logout":{"verb":"GET","url":"/api/v1/account/logout","args":[]},"updatePassword":{"verb":"PUT","url":"/api/v1/account/update-password","args":["password"]},"updateProfile":{"verb":"PUT","url":"/api/v1/account/update-profile","args":["fullName","email","username"]},"updateNotifications":{"verb":"PUT","url":"/api/v1/account/update-notifications","args":["isMailingListAccepted"]},"delete":{"verb":"POST","url":"/api/v1/account/delete","args":["username"]},"login":{"verb":"PUT","url":"/api/v1/entrance/login","args":["username","password","rememberMe"]},"signup":{"verb":"POST","url":"/api/v1/entrance/signup","args":["username","email","password","fullName","isPrivacyPolicyAccepted","isMailingListAccepted"]},"sendVerifyEmail":{"verb":"GET","url":"/api/v1/entrance/send-verify-email","args":[]},"sendPasswordRecoveryEmail":{"verb":"POST","url":"/api/v1/entrance/send-password-recovery-email","args":["email"]},"updatePasswordAndLogin":{"verb":"POST","url":"/api/v1/entrance/update-password-and-login","args":["password","token"]},"deliverContactFormMessage":{"verb":"POST","url":"/api/v1/deliver-contact-form-message","args":["email","fullName","message"]},"observeMySession":{"verb":"POST","url":"/api/v1/observe-my-session","args":[],"protocol":"io.socket"},"payToUser":{"verb":"POST","url":"/api/v1/pay/user","args":["payeeId","payeeUsername","amount","reason"]},"isAlreadyRegistered":{"verb":"POST","url":"/api/v1/account/is-already-registered","args":["email","username"]},"deleteUser":{"verb":"POST","url":"/api/v1/user/delete","args":["id"]},"switchAdmin":{"verb":"POST","url":"/api/v1/user/switch-admin","args":["id"]},"createAttendance":{"verb":"POST","url":"/api/v1/attendance/create","args":["attendanceTitle","attendanceFee","sendAttendanceFeeAt","sendAttendanceFeeAtTime"]},"deleteAttendance":{"verb":"POST","url":"/api/v1/attendance/delete","args":["id"]}}
  /* eslint-enable */

});
