/**
 * <clipboard-text>
 * -----------------------------------------------------------------------------
 * Save text to clipboard
 *
 * @type {Component}
 * -----------------------------------------------------------------------------
 */

parasails.registerComponent('clipboardText', {

  //  ╔═╗╦═╗╔═╗╔═╗╔═╗
  //  ╠═╝╠╦╝║ ║╠═╝╚═╗
  //  ╩  ╩╚═╚═╝╩  ╚═╝
  props: [
    'title',
    'content',
    'label'
  ],

  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: function (){
    return {
      copied: false,
      upHere: false
    };
  },

  //  ╦ ╦╔╦╗╔╦╗╦
  //  ╠═╣ ║ ║║║║
  //  ╩ ╩ ╩ ╩ ╩╩═╝
  template: `
    <button class="btn" 
            :class="{'fw-bolder':upHere, 'fw-normal':!upHere, 'btn-outline-secondary':label, 'btn-outline-link p-0 mx-1 my-0':!label}" 
            :title="title" 
            @click="copyToClipboard(content)" 
            @mouseover="upHere = !label" 
            @mouseleave="upHere = false">
      <span v-if="label">{{ title }}</span>
      <i class="fa fa-copy" v-if="!copied"></i>
      <i class="fa fa-check" v-if="copied"></i>
    </button>
  `,

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    if (this.content === undefined || this.title === undefined) {
      throw new Error('Incomplete usage of <clipboard-text>:  Please specify `title`, `content`. For example: `<clipboard-text title="Lorem ipsum" content="Text to copy" [label="true"] />`');
    }
  },

  beforeDestroy: function() {

  },

  watch: {

  },


  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {

    copyToClipboard: async function(content) {
      await navigator.clipboard.writeText(content);
      this.copied = true;
      setTimeout(()=>{this.copied = false;}, 1000);
    },

  }

});

