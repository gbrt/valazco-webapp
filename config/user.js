/**
 * User configuration
 * (sails.config.user)
 */

module.exports.user = {

  valazcoUser: 1,

  reservedUsernames: [
    'valazco',
    'login',
    'logout',
    'pay',
    'signup',
    'faq',
    'contact',
    'admin',
    'user',
    'password',
    'api',
    'account',
    'info',
    'list',
    'add',
    'group',
    'transaction',
    'attendance',
    'attendee',
    'member'
  ]
  
};
