/**
 * Seed Function
 * (sails.config.bootstrap)
 *
 * A function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also create a hook.
 *
 * For more information on seeding your app with fake data, check out:
 * https://sailsjs.com/config/bootstrap
 */

module.exports.bootstrap = async function() {

  // Import dependencies
  var path = require('path');

  // This bootstrap version indicates what version of fake data we're dealing with here.
  var HARD_CODED_DATA_VERSION = 0;

  // This path indicates where to store/look for the JSON file that tracks the "last run bootstrap info"
  // locally on this development computer (if we happen to be on a development computer).
  var bootstrapLastRunInfoPath = path.resolve(sails.config.appPath, '.tmp/bootstrap-version.json');

  // Whether or not to continue doing the stuff in this file (i.e. wiping and regenerating data)
  // depends on some factors:
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  // If the hard-coded data version has been incremented, or we're being forced
  // (i.e. `--drop` or `--environment=test` was set), then run the meat of this
  // bootstrap script to wipe all existing data and rebuild hard-coded data.
  if (sails.config.models.migrate !== 'drop' && sails.config.environment !== 'test') {
    // If this is _actually_ a production environment (real or simulated), or we have
    // `migrate: safe` enabled, then prevent accidentally removing all data!
    if (process.env.NODE_ENV==='production' || sails.config.models.migrate === 'safe') {
      sails.log('Since we are running with migrate: \'safe\' and/or NODE_ENV=production (in the "'+sails.config.environment+'" Sails environment, to be precise), skipping the rest of the bootstrap to avoid data loss...');
      return;
    }//•

    // Compare bootstrap version from code base to the version that was last run
    var lastRunBootstrapInfo = await sails.helpers.fs.readJson(bootstrapLastRunInfoPath)
    .tolerate('doesNotExist');// (it's ok if the file doesn't exist yet-- just keep going.)

    if (lastRunBootstrapInfo && lastRunBootstrapInfo.lastRunVersion === HARD_CODED_DATA_VERSION) {
      sails.log('Skipping v'+HARD_CODED_DATA_VERSION+' bootstrap script...  (because it\'s already been run)');
      sails.log('(last run on this computer: @ '+(new Date(lastRunBootstrapInfo.lastRunAt))+')');
      return;
    }//•

    sails.log('Running v'+HARD_CODED_DATA_VERSION+' bootstrap script...  ('+(lastRunBootstrapInfo ? 'before this, the last time the bootstrap ran on this computer was for v'+lastRunBootstrapInfo.lastRunVersion+' @ '+(new Date(lastRunBootstrapInfo.lastRunAt)) : 'looks like this is the first time the bootstrap has run on this computer')+')');
  }
  else {
    sails.log('Running bootstrap script because it was forced...  (either `--drop` or `--environment=test` was used)');
  }

  // Since the hard-coded data version has been incremented, and we're running in
  // a "throwaway data" environment, delete all records from all models.
  for (let identity in sails.models) {
    await sails.models[identity].destroy({});
  }//∞
	
	sails.log('Creating fake users...');
  
  let fakeUsers = [
    {
			username: 'valazco',
			email: 'localhost@valazco.it',
			fullName: 'VALAZCO',
			role: 'valazco',
			notifications: sails.config.roles.valazco.notifications,
			password: await sails.helpers.passwords.hashPassword('asd'),
			funds: sails.config.val.initialAmount,
			active: false,
			decreaseAt: Date.now()+Math.floor(Math.random()*1000),
			isPrivacyPolicyAccepted: true
		},
    {
			username: 'asd',
			email: 'asd@asd.asd',
			fullName: 'Asdino',
			role: 'founder',
			notifications: sails.config.roles.founder.notifications,
			password: await sails.helpers.passwords.hashPassword('asd'),
			funds: 9999999999,
			active:true,
			decreaseAt: Date.now()+Math.floor(Math.random()*1000),
			isPrivacyPolicyAccepted: true
		},
    {
      username: 'asd1000',
			email: 'asd1000@asd.asd',
			fullName: 'Asdino1000',
			role: 'admin',
			notifications: sails.config.roles.admin.notifications,
			password: await sails.helpers.passwords.hashPassword('asd'),
			funds: 10,
			active:true,
			decreaseAt: Date.now()+Math.floor(Math.random()*1000),
			isPrivacyPolicyAccepted: true
		}
	];
		
	for (let i = 0; i < 30; i++){
	  const notifications = { ...sails.config.roles.default.notifications };
	  
	  notifications.mailingList = Math.random() < 0.3 ? true : false;
	  
	  fakeUsers.push({
			username: 'fake'+i,
			email: 'fake'+i+'@fakeuser.com',
			fullName: 'Fake ' + i,
			emailStatus: Math.random() < 0.5 ? 'confirmed' : 'unconfirmed',
			password: await sails.helpers.passwords.hashPassword('asd'),
			funds: Math.floor(Math.random() * (sails.config.val.initialAmount*5 - 0.1 + 1) + 0.1), 
			active: true,
			decreaseAt: Date.now()+Math.floor(Math.random()*1000),
			isPrivacyPolicyAccepted: Math.random() < 0.9 ? true : false,
			notifications: notifications
		});
	}
	
  await User.createEach(fakeUsers);
  
  sails.log('Creating fake transactions...');
  
  let fakeTransactions = [];
  let dateToday = await sails.helpers.date.dayStart();

  for (let i = 2; i <= fakeUsers.length; i++) {
    let tNum = Math.floor(Math.random() * (25 - 2 + 1) + 2);
    let lastTransactionAt = 0;
    
    for (let j = 0; j < tNum; j++) {
      let u = Math.floor(Math.random() * (fakeUsers.length - 2 + 1) + 2);
      
      if (u === i) continue;
      
      let tIn = Math.random() < 0.5 ? true : false;
      let userId = tIn ? u : i;
      let payeeId = tIn ? i : u;
      
      let amount = Math.trunc((Math.random() * (2 - 0.01 + 1) + 0.01) * 100) / 100;
      let reason = 'test '+i+'-'+j;
      
      let maxDate = dateToday - 1 * 24 * 60 * 60 * 1000;
      let minDate = maxDate - 120 * 24 * 60 * 60 * 1000;
      let createdAt = Math.random() * (maxDate - minDate + 1) + minDate;
      
      if (createdAt > lastTransactionAt) {
        lastTransactionAt = createdAt;
      }
      
      let transaction = {
        amount: amount,
        reason: reason,
        from: userId,
        to: payeeId,
        createdAt: createdAt
      };
      
      fakeTransactions.push(transaction);
      
      await sails.helpers.user.updateStatusAfterTransaction(userId, lastTransactionAt);
      await sails.helpers.user.updateStatusAfterTransaction(payeeId, lastTransactionAt);
    }
  }
  
  fakeTransactions.sort(function(a, b){
    return a.createdAt - b.createdAt;
  });
  await sails.config.cron.deactivateUserDailyFunds.onTick();
  await Transaction.createEach(fakeTransactions);

  // Save new bootstrap version
  await sails.helpers.fs.writeJson.with({
    destination: bootstrapLastRunInfoPath,
    json: {
      lastRunVersion: HARD_CODED_DATA_VERSION,
      lastRunAt: Date.now()
   
		},
    force: true
  })
  .tolerate((err)=>{
    sails.log.warn('For some reason, could not write bootstrap version .json file.  This could be a result of a problem with your configured paths, or, if you are in production, a limitation of your hosting provider related to `pwd`.  As a workaround, try updating app.js to explicitly pass in `appPath: __dirname` instead of relying on `chdir`.  Current sails.config.appPath: `'+sails.config.appPath+'`.  Full error details: '+err.stack+'\n\n(Proceeding anyway this time...)');
  });

};
