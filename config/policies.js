/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  '*': 'is-logged-in',

  'admin/*': 'is-admin',

  // Bypass the `is-logged-in` policy for:
  'view-homepage': true,
  'entrance/*': true,
  'account/logout': true,
  'account/is-already-registered': true,
  'view-faq': true,
  'view-contact': true,
  'deliver-contact-form-message': true,

};
