/**
 * Edu environment settings
 * (sails.config.*)
 *
 * This is mostly a carbon copy of the production environment settings
 * in config/env/production.js, but with the overrides listed below.
 * For more detailed information and links about what these settings do
 * see the production config file.
 *
 * > This file takes effect when `sails.config.environment` is "Edu".
 * > But note that NODE_ENV should still be "production" when lifting
 * > your app in the Edu environment.  In other words:
 * > ```
 * >     NODE_ENV=production sails_environment=Edu node app
 * > ```
 *
 * If you're unsure or want advice, stop by:
 * https://sailsjs.com/support
 */

var PRODUCTION_CONFIG = require('./production');
//--------------------------------------------------------------------------
// /\  Start with your production config, even if it's just a guess for now,
// ||  then configure your Edu environment afterwards.
//     (That way, all you need to do in this file is set overrides.)
//--------------------------------------------------------------------------

module.exports = Object.assign({}, PRODUCTION_CONFIG, {

  datastores: Object.assign({}, PRODUCTION_CONFIG.datastores, {
    default: Object.assign({}, PRODUCTION_CONFIG.datastores.default, {
      // url: 'mysql://shared:some_password_everyone_knows@db.example.com:3306/my_Edu_db',
      //--------------------------------------------------------------------------
      // /\  Hard-code your Edu db `url`.
      // ||  (or use system env var: `sails_datastores__default__url`)
      //--------------------------------------------------------------------------
    })
  }),

  sockets: Object.assign({}, PRODUCTION_CONFIG.sockets, {

    onlyAllowOrigins: [
      'https://edu.valazco.it',
    ],
    //--------------------------------------------------------------------------
    // /\  Hard-code a Edu-only override for allowed origins.
    // ||  (or set this array via JSON-encoded system env var)
    //     ```
    //     sails_sockets__onlyAllowOrigins='["http://localhost:1337", "…"]'
    //     ```
    //--------------------------------------------------------------------------

    // url: 'redis://shared:some_password_everyone_knows@bigsquid.redistogo.com:9562/',
    //--------------------------------------------------------------------------
    // /\  Hard-code your Edu Redis server's `url`.
    // ||  (or use system env var: `sails_sockets__url`)
    //--------------------------------------------------------------------------
  }),

  session: Object.assign({}, PRODUCTION_CONFIG.session, {
    // url: 'redis://shared:some_password_everyone_knows@bigsquid.redistogo.com:9562/Edu-sessions',
    //--------------------------------------------------------------------------
    // /\  Hard-code your Edu Redis server's `url` again here.
    // ||  (or use system env var: `sails_session__url`)
    //--------------------------------------------------------------------------
  }),

  custom: Object.assign({}, PRODUCTION_CONFIG.custom, {

    baseUrl: 'https://edu.valazco.it',
    //--------------------------------------------------------------------------
    // /\  Hard-code the base URL where your Edu environment is hosted.
    // ||  (or use system env var: `sails_custom__baseUrl`)
    //--------------------------------------------------------------------------

    internalEmailAddress: 'support+edu@valazco.it',
    //--------------------------------------------------------------------------
    // /\  Hard-code the email address that should receive support/contact form
    // ||  messages in Edu (or use `sails_custom__internalEmailAddress`)
    //--------------------------------------------------------------------------

  })

});
