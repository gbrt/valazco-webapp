module.exports.cron = {

  deactivateUserDailyFunds: {

    schedule: '0 0 0 * * *',

    onTick: async function () {
      let dateNow = await sails.helpers.date.dayEnd();
      let users = await User.find().where({'active': true, 'deactivateAt':{'<=': dateNow}});

      async function deactivateUser (user) {
        await User.updateOne(user.id).set({
          'active': false,
          'activateAt': 0
        });
      }

      users.forEach(deactivateUser);
    }

  },

  reactivateUserDailyFunds: {

    schedule: '0 0 1 * * *',

    onTick: async function () {
      let dateNow = await sails.helpers.date.dayEnd();
      let deactivateAt = await sails.helpers.user.calculateDeactivateAt(dateNow);

      let users = await User.find().where({
        'active': false,
        'activateAt':{'<=': dateNow}
      });

      async function reactivateUser (user) {
        if(user.activateAt > 0) {
          await User.updateOne(user.id).set({
            'active': true,
            'deactivateAt': deactivateAt,
            'activateAt': 0
          });
        }
      }

      users.forEach(reactivateUser);
    }

  },

  dailyFunds: {

    schedule: '0 0 2 * * *',

    onTick: async function () {
      let users = await User.find({active: true});

      async function creditUser (user) {
        await sails.helpers.transaction.payin(user.id, sails.config.val.dailyAmount);
      }

      users.forEach(creditUser);
    }

  },

  decreaseUserFunds: {

    schedule: '0 0 3 * * *',

    onTick: async function () {
      let decreaseNow = await sails.helpers.date.dayStart();
      let nextDecreaseTime = await sails.helpers.user.calculateDecreaseAt(decreaseNow);
      let decreasePercentage = sails.config.val.decreasePercentage;

      let users = await User.find().where({
        'decreaseAt':{'<=': decreaseNow}
      });

      async function decreaseUserFunds (user) {
        let decreaseAmount = Math.round(((user.funds / 100) * decreasePercentage) * 100) / 100;

        if (decreaseAmount >= 0.01) {
          await sails.helpers.transaction.create(
            user.id,
            sails.config.user.valazcoUser,
            decreaseAmount,
            '-'+decreasePercentage+'%'
          );
        }

        await User.updateOne(user.id).set({
          'lastDecreaseAt': decreaseNow,
          'decreaseAt': nextDecreaseTime
        });
      }

      users.forEach(decreaseUserFunds);
    }

  },
  
  sendAttendanceFee: {
    
    schedule: '0 0 */4 * * *',
    
    onTick: async function () {
      let dateNow = Date.now();
      let attendances = await Attendance.find().where({
        'sendAttendanceFeeAt':{'<=': dateNow},
        'isAttendanceFeeSent': false
      }).populate('attendees');
      
      async function sendFee(attendance) {
        attendance.attendees.forEach(async (user) => {
          await sails.helpers.attendance.sendAttendanceFee(attendance.id, user.id);
        });
        await Attendance.updateOne(attendance.id).set({'isAttendanceFeeSent': true});
      }
      
      attendances.forEach(sendFee);
    }
    
  },

  updateStats: {

    schedule: '0 0 4 * * *',

    onTick: async function () {
      await sails.helpers.stats.createStats();
    }

  },

};

