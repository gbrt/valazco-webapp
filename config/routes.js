/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  //  ╦ ╦╔═╗╔╗ ╔═╗╔═╗╔═╗╔═╗╔═╗
  //  ║║║║╣ ╠╩╗╠═╝╠═╣║ ╦║╣ ╚═╗
  //  ╚╩╝╚═╝╚═╝╩  ╩ ╩╚═╝╚═╝╚═╝
  'GET /':                   { action: 'view-homepage' },

  'GET /pay/:username?':     { action: 'transaction/view-pay' },
  'GET /~:username?':        { action: 'transaction/view-pay' },

  'GET /faq':                { action:   'view-faq' },
  'GET /contact':            { action:   'view-contact' },

  'GET /signup':             { action: 'entrance/view-signup' },
  'GET /email/confirm':      { action: 'entrance/confirm-email' },
  'GET /email/confirmed':    { action: 'entrance/view-confirmed-email' },

  'GET /login':              { action: 'entrance/view-login' },
  'GET /password/forgot':    { action: 'entrance/view-forgot-password' },
  'GET /password/new':       { action: 'entrance/view-new-password' },

  'GET /account':               { action: 'account/view-account-overview' },
  'GET /account/dashboard':     { action: 'account/view-dashboard' },
  'GET /account/password':      { action: 'account/view-edit-password' },
  'GET /account/profile':       { action: 'account/view-edit-profile' },
  'GET /account/notifications': { action: 'account/view-edit-notifications' },
  
  'GET /1at:token':          { action: 'attendance/add-user' },


  // ADMIN
  'GET /admin/user':         { action: 'admin/user/list' },
  'GET /admin/user/:id':     { action: 'admin/user/view' },
  
  'GET /admin/transaction':  { action: 'admin/transaction/list' },
  
  'GET /admin/system-info':  { action: 'admin/view-system-info' },
  
  'GET /admin/attendance':        { action: 'admin/attendance/list' },
  'GET /admin/attendance/:id':    { action: 'admin/attendance/view' },
  'GET /admin/attendance/:id/add/:userId':     { action: 'admin/attendance/add-user-to-attendance' },


  //  ╔╦╗╦╔═╗╔═╗  ╦═╗╔═╗╔╦╗╦╦═╗╔═╗╔═╗╔╦╗╔═╗   ┬   ╔╦╗╔═╗╦ ╦╔╗╔╦  ╔═╗╔═╗╔╦╗╔═╗
  //  ║║║║╚═╗║    ╠╦╝║╣  ║║║╠╦╝║╣ ║   ║ ╚═╗  ┌┼─   ║║║ ║║║║║║║║  ║ ║╠═╣ ║║╚═╗
  //  ╩ ╩╩╚═╝╚═╝  ╩╚═╚═╝═╩╝╩╩╚═╚═╝╚═╝ ╩ ╚═╝  └┘   ═╩╝╚═╝╚╩╝╝╚╝╩═╝╚═╝╩ ╩═╩╝╚═╝
  '/logout':                  '/api/v1/account/logout',


  //  ╦ ╦╔═╗╔╗ ╦ ╦╔═╗╔═╗╦╔═╔═╗
  //  ║║║║╣ ╠╩╗╠═╣║ ║║ ║╠╩╗╚═╗
  //  ╚╩╝╚═╝╚═╝╩ ╩╚═╝╚═╝╩ ╩╚═╝
  // …


  //  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
  //  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
  //  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝
  // Note that, in this app, these API endpoints may be accessed using the `Cloud.*()` methods
  // from the Parasails library, or by using those method names as the `action` in <ajax-form>.
  '/api/v1/account/logout':                               { action: 'account/logout' },
  'PUT   /api/v1/account/update-password':                { action: 'account/update-password' },
  'PUT   /api/v1/account/update-profile':                 { action: 'account/update-profile' },
  'PUT   /api/v1/account/update-notifications':           { action: 'account/update-notifications' },
  'POST  /api/v1/account/delete':                         { action: 'account/delete' },
  'PUT   /api/v1/entrance/login':                         { action: 'entrance/login' },
  'POST  /api/v1/entrance/signup':                        { action: 'entrance/signup' },
  'GET   /api/v1/entrance/send-verify-email':             { action: 'entrance/send-verify-email' },
  'POST  /api/v1/entrance/send-password-recovery-email':  { action: 'entrance/send-password-recovery-email' },
  'POST  /api/v1/entrance/update-password-and-login':     { action: 'entrance/update-password-and-login' },
  'POST  /api/v1/deliver-contact-form-message':           { action: 'deliver-contact-form-message' },
  'POST  /api/v1/observe-my-session':                     { action: 'observe-my-session', hasSocketFeatures: true },

  'POST /api/v1/pay/user':                                { action: 'transaction/pay-to-user' },

  'POST /api/v1/account/is-already-registered':           { action: 'account/is-already-registered' },
  'POST /api/v1/user/delete':                             { action: 'admin/user/delete-user' },
  'POST /api/v1/user/switch-admin':                       { action: 'admin/user/switch-admin' },
  
  'POST /api/v1/attendance/create':                       { action: 'admin/attendance/create-attendance' },
  'POST /api/v1/attendance/delete':                       { action: 'admin/attendance/delete-attendance' }

};
