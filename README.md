## Run in local
Install node.js >= v8.17.0

> npm install sails -g
> git clone https://codeberg.org/valazco/webapp.git
> cd webapp
> npm install
> sails lift

The app will run at http://localhost:1337


## Run in PROD (with podman)

> git clone https://codeberg.org/valazco/webapp.git
> cd webapp
> npm install
> podman build -t valazco .
> cp kube.yml.dist kube.yml
> podman kube play kube.yml

### static pages

> cd views/pages/
> git clone https://codeberg.org/valazco/webapp-static-pages.git locales


## (optional) Caddy server

> mkdir caddy
> nano caddy/Caddyfile

 ```
valazco.it {
        reverse_proxy valazco.it:1337
}
```
> sudo sysctl net.ipv4.ip_unprivileged_port_start=80
> podman run -d --name caddy-server --restart=always -v $(pwd)/Caddyfile:/etc/caddy/Caddyfile:Z -p 80:80 -p 443:443 caddy
> sudo sysctl net.ipv4.ip_unprivileged_port_start=1024
