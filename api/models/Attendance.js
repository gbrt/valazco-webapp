/**
 * Attendance.js
 */

module.exports = {

  attributes: {
  
    token: {
      type: 'string',
      unique: true,
      required: true
    },
  
    title: {
      type: 'string',
      required: true
    },
    
    attendanceFee: {
      type: 'number',
      defaultsTo: 0
    },
    
    isAttendanceFeeSent: {
      type: 'boolean',
      defaultsTo: false
    },
    
    sendAttendanceFeeAt: {
      type: 'number',
      description: 'A JS timestamp (epoch ms) representing the attendanceFee paying time.',
      example: 1502844074211,
      defaultsTo: 0
    },

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    
    attendees: {
      collection: 'user',
      via: 'attendances'
    },
    
    owner: {
      model: 'user'
    }

  },

};

