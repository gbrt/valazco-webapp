module.exports = {


  friendlyName: 'View users',


  description: 'Show users.',

  inputs: {

    page: {
      type: 'number'
    },
    
    active: {
      type: 'boolean'
    }

  },


  exits: {

    success: {
      viewTemplatePath: 'pages/admin/user/list',
      description: 'Display users.'
    },

  },


  fn: async function (inputs, exits) {    
    let filter = {};
    
    if (inputs.active !== undefined) {
      filter['active'] = inputs.active;
    }

    let pagination = await sails.helpers.pagination.with({
      model: User, 
      selectedPage: inputs.page, 
      paginationSize: sails.config.pagination.adminPaginationSize,
      where: filter
    });

    let foundUsers = await User.find()
      .skip(pagination.start)
      .limit(pagination.size)
      .sort([{id:'ASC'}]);

    return exits.success({
      users: foundUsers,
      pagination: pagination,
      role: sails.config.roles[this.req.me.role]
    });

  }


};
