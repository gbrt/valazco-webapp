module.exports = {


  friendlyName: 'Delete user',


  description: 'Search and delete user by id',


  inputs: {

    id: {
      type: 'number',
      required: true
    }

  },

  exits: {

    success: {
      description: 'Successfully deleted user.'
    },

    notFound: {
      description: 'User not found.'
    },
    
    forbidden: {
      description: 'Insufficient permissions'
    }

  },


  fn: async function (inputs, exits) {
    if (!sails.config.roles[this.req.me.role].deleteUsers) {
      throw 'forbidden';
    }

    let user = await User.destroyOne(inputs.id).fetch();

    if(!user) {
      throw 'notFound';
    }

    return exits.success({
      user,
    });

  }


};
