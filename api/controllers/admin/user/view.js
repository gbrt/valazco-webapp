module.exports = {


  friendlyName: 'View user',


  description: 'Show user info.',

  inputs: {

    id: {
      type: 'number',
    },

    page: {
      type: 'number'
    }

  },


  exits: {

    success: {
      viewTemplatePath: 'pages/admin/user/view',
      description: 'Display users.'
    },

  },


  fn: async function (inputs, exits) {

    let user = await User.findOne(inputs.id ? inputs.id : this.req.session.userId).populate('attendances');

    let transactionFilter = {
      or: [{from: user.id}, {to: user.id}]
    };

    let pagination = await sails.helpers.pagination.with({
      model: Transaction,
      selectedPage: inputs.page,
      paginationSize: sails.config.pagination.userDefaultPaginationSize,
      where: transactionFilter
    });

    let foundTransactions = await Transaction.find()
      .where(transactionFilter)
      .skip(pagination.start)
      .limit(pagination.size)
      .sort([{createdAt: 'DESC'}])
      .populate('from').populate('to');

    let transactions = [];

    for (let i = 0; i < foundTransactions.length; i++){
      let t = foundTransactions[i];
      let isPayin = await sails.helpers.transaction.isPayin(user, t);
      let u = isPayin ? t.from : t.to;

      transactions.push({
        id: t.id,
        date: t.createdAt,
        amount: t.amount,
        reason: t.reason,
        isPayin: isPayin,
        withUser: u
      });
    }

    return exits.success({
      user: user,
      transactions: transactions,
      pagination: pagination,
      role: sails.config.roles[this.req.me.role]
    });

  }


};
