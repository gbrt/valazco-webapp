module.exports = {


  friendlyName: 'View transactions',


  description: 'Show transactions.',

  inputs: {

    page: {
      type: 'number'
    }

  },


  exits: {

    success: {
      viewTemplatePath: 'pages/admin/transaction/list',
      description: 'Display transactions.'
    },

  },


  fn: async function (inputs, exits) {
    let pagination = await sails.helpers.pagination(
      Transaction, inputs.page, sails.config.pagination.adminPaginationSize
    );

    let foundTransactions = await Transaction.find()
      .populate('from').populate('to')
      .skip(pagination.start)
      .limit(pagination.size)
      .sort([{id:'DESC'}]);

    return exits.success({
      transactions: foundTransactions,
      pagination: pagination
    });

  }


};
