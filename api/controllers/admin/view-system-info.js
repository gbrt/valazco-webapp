module.exports = {


  friendlyName: 'View system info',


  description: 'Show system info.',

  inputs: {


  },


  exits: {

    success: {
      viewTemplatePath: 'pages/admin/system-info',
      description: 'Display system info.'
    },

  },


  fn: async function () {

    let initialAmount = sails.config.val.initialAmount;
    let dailyAmount = sails.config.val.dailyAmount;
    let maxInactivity = await sails.helpers.date.durationDays(sails.config.val.maxInactivity);
    let reactivateTime = await sails.helpers.date.durationDays(sails.config.val.reactivateTime);
    let decreasePercentage = sails.config.val.decreasePercentage;
    let decreaseTime = await sails.helpers.date.durationDays(sails.config.val.decreaseTime);

    await sails.helpers.stats.createStats();

    // Respond with view.
    return {
      initialAmount: initialAmount,
      dailyAmount: dailyAmount,
      maxInactivity: maxInactivity,
      reactivateTime: reactivateTime,
      decreasePercentage: decreasePercentage,
      decreaseTime: decreaseTime
    };

  }


};
