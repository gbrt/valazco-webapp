module.exports = {


  friendlyName: 'View attendance',


  description: 'Show attendance info.',

  inputs: {

    id: {
      type: 'number',
    },

    page: {
      type: 'number'
    }

  },


  exits: {

    success: {
      viewTemplatePath: 'pages/admin/attendance/view',
      description: 'Display attendance.'
    },

  },


  fn: async function (inputs, exits) {

    let attendance = await Attendance.findOne(inputs.id).populate('attendees');

    return exits.success({
      attendance: attendance
    });

  }


};
