module.exports = {


  friendlyName: 'Delete attendance',


  description: 'Search and delete attendance by id',


  inputs: {

    id: {
      type: 'number',
      required: true
    }

  },

  exits: {

    success: {
      description: 'Successfully deleted attendance.'
    },

    notFound: {
      description: 'Attendance not found.'
    }

  },


  fn: async function (inputs, exits) {

    let attendance = await Attendance.destroyOne(inputs.id).fetch();

    if(!attendance) {
      throw 'notFound';
    }

    return exits.success({
      attendance,
    });

  }


};
