module.exports = {


  friendlyName: 'Create attendance',


  description: '',


  inputs: {

    attendanceTitle: {
      type: 'string',
      required: true
    },
    
    attendanceFee: {
      type: 'number',
      defaultsTo: 0
    },
    
    sendAttendanceFeeAt: {
      type: 'string',
      defaultsTo: ''
    },
    
    sendAttendanceFeeAtTime: {
      type: 'string',
      defaultsTo: '00:00:00'
    }

  },

  exits: {

    success: {
      description: 'Successfully created group.'
    }

  },


  fn: async function (inputs, exits) {
    const sendAttendanceFeeAt = new Date(inputs.sendAttendanceFeeAt + 'T' + inputs.sendAttendanceFeeAtTime);

    await sails.helpers.attendance.create(inputs.attendanceTitle, inputs.attendanceFee, sendAttendanceFeeAt);
    
    
    return exits.success();
  }


};
