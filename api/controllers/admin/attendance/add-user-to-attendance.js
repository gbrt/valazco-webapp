module.exports = {


  friendlyName: 'Add user to attendance',


  description: 'Add user to attendance.',

  inputs: {

    id: {
      type: 'number',
      require: true
    },
    
    userId: {
      type: 'number',
      require: true
    },

  },


  exits: {

    success: {
      description: 'Added user attendance.',
      responseType: 'redirect'
    },

  },


  fn: async function (inputs, exits) {    

    let attendance = await Attendance.findOne(inputs.id);
    let user = await User.findOne(inputs.userId);
    
    await sails.helpers.attendance.addUser(attendance.id, user.id);

    return exits.success('/admin/attendance');

  }


};
