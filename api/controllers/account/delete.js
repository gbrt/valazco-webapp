module.exports = {


  friendlyName: 'Delete',


  description: 'Delete account.',


  inputs: {
    
    username: {
      type: 'string',
      required: true
    }
    
  },


  exits: {
    
    success: {
    
    },
    
    wrongUser: {
      
    }

  },


  fn: async function (inputs, exits) {
    if (inputs.username !== this.req.me.username) {
      throw 'wrongUser';
    }
    
    let user = await User.destroyOne({username: inputs.username}).fetch();

    if(!user) {
      throw 'wrongUser';
    }
    
    // All done.
    return exits.success({});
  }


};
