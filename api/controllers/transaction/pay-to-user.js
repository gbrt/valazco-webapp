module.exports = {


  friendlyName: 'Send value',


  description: 'Send value to user and create new transaction',


  extendedDescription:
`Create new transaction`,


  inputs: {

    payeeId: {
      type: 'ref',

      required: false
    },

    payeeUsername: {
      description: 'The username of the user to look up.',

      type: 'string',

      required: false
    },

    amount: {
      description: 'The amount to transfer',

      type: 'number',

      required: true
    },

    reason: {
      description: 'Reason of the transaction',

      type: 'string'
    },

  },


  exits: {

    success: {
      description: 'Payment created successfully.'
    },

    notFound: {
      description: 'User not found.'
    },

    sameUser: {
      description: 'Cannot pay to yourself.'
    },

    insufficientFunds: {
      description: 'Insufficient funds.'
    }

  },


  fn: async function ({payeeId, payeeUsername, amount, reason}) {
    let user = await User.findOne(this.req.session.userId);
    let userSearchCriteria = payeeId ? {'id': payeeId} : {'username': payeeUsername};
    let payee = await User.findOne(userSearchCriteria);

    if(!user || !payee) {
      throw 'notFound';
    }

    if(user.id === payee.id) {
      throw 'sameUser';
    }

    await sails.helpers.transaction.create(user.id, payee.id, amount, reason)
                                   .intercept('insufficientFunds', 'insufficientFunds');

    return {

    };

  }

};
