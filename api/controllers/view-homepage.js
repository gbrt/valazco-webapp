module.exports = {


  friendlyName: 'View homepage or redirect',


  description: 'Display or redirect to the appropriate homepage, depending on login status.',

  inputs: {

    page: {
      type: 'number'
    },
    
    attendance: {
      type: 'string'
    }

  },

  exits: {

    success: {
      statusCode: 200,
      description: 'Requesting user is a guest, so show the public landing page.',
      viewTemplatePath: 'pages/homepage'
    },

    redirect: {
      description: 'The user is loggedin.',
      extendedDescription: 'Redirect to dashboard.',
      responseType: 'redirect'
    }

  },


  fn: async function (inputs, exits) {
  
    if (this.req.me) {
      throw {redirect: '/account/dashboard'};
    }

    return exits.success({
      locale: this.req.getLocale()
    });

  }
  
  
};
