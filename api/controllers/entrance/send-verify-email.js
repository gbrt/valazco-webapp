module.exports = {


  friendlyName: 'Send verify email',


  description: '',


  inputs: {

  },


  exits: {
    success: {
      responseType: 'redirect'
    }
  },


  fn: async function (inputs) {
    await sails.helpers.email.sendVerifyEmail(this.req.session.userId).tolerate('notFound');
    // All done.
    return '/';
  }


};
