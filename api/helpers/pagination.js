module.exports = {


  friendlyName: 'Pagination',


  description: 'Pagination utility.',


  inputs: {

    model: {
      type: 'ref'
    },

    selectedPage: {
      type: 'number'
    },

    paginationSize: {
      type: 'number'
    },

    where: {
      type: 'ref'
    }

  },

  exits: {

    success: {
      description: 'The requesting socket is now subscribed to socket broadcasts about the logged-in user\'s session.',
    },

  },

  fn: async function (inputs, exits) {
    
    let where = inputs.where || {};
    let itemsCount = await inputs.model.count().where(where);
    let paginationSize = inputs.paginationSize ? inputs.paginationSize : sails.config.pagination.paginationSize;
    let pages = 1;
    let page = 1;
    let paginationStart = 0;
    let selectedPage = inputs.selectedPage;
    let prevPage = null;
    let nextPage = null;

    if(itemsCount > paginationSize) {
      pages = Math.ceil(itemsCount / paginationSize);
      page = !selectedPage || selectedPage < 1 || selectedPage > pages ? 1 : selectedPage;
      paginationStart = (page - 1) * paginationSize;
      prevPage = page > 1 ? page -1 : 1;
      nextPage = page < pages ? page + 1 : pages;
    }
    
    return exits.success({
      size: paginationSize,
      pages: pages,
      page: page,
      start: paginationStart,
      prevPage: prevPage !== page ? prevPage : null,
      nextPage: nextPage !== page ? nextPage : null
    });
    
  }


};
