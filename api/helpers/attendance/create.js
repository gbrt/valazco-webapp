module.exports = {


  friendlyName: 'Create',


  description: 'Create attendance.',


  inputs: {
  
    title: {
      type: 'string',
      required: true
    },
    
    attendanceFee: {
      type: 'number',
      defaultsTo: 0
    },
    
    sendAttendanceFeeAt: {
      type: 'number',
      description: 'A JS timestamp (epoch ms) representing the attendanceFee paying time.',
      example: 1502844074211,
      defaultsTo: 0
    },
    
    owner: {
      type: 'number'
    }
    
  },


  fn: async function (inputs) {
    const token = await sails.helpers.strings.random('url-friendly');
  
    await Attendance.create({
      'title': inputs.title,
      'attendanceFee': inputs.attendanceFee,
      'sendAttendanceFeeAt': inputs.sendAttendanceFeeAt,
      'token': token,
      'owner': inputs.owner || sails.config.user.valazcoUser 
    });
    
  }


};

