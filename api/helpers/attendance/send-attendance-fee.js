module.exports = {


  friendlyName: 'Add user',


  description: '',


  inputs: {
    
    attendanceId: {
      description: 'The ID of the group to look up.',

      type: 'number',

      required: true
    },
    
    userId: {
      description: 'The ID of the user to look up.',

      type: 'number',

      required: true
    }
    
  },


  fn: async function (inputs) {
    let user = await User.findOne(inputs.userId);
    let attendance = await Attendance.findOne(inputs.attendanceId);
  
    await sails.helpers.transaction.create(
      sails.config.user.valazcoUser,
      user.id,
      attendance.attendanceFee,
      attendance.title
    );   
  }


};

