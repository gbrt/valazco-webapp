module.exports = {


  friendlyName: 'Add user',


  description: '',


  inputs: {
    
    groupId: {
      description: 'The ID of the group to look up.',

      type: 'number',

      required: true
    },
    
    userId: {
      description: 'The ID of the user to look up.',

      type: 'number',

      required: true
    }
    
  },


  fn: async function (inputs) {
  
    await Attendance.addToCollection(inputs.groupId, 'attendees', inputs.userId);
    
  }


};

