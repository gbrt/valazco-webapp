module.exports = {


  friendlyName: 'Get day end',


  description: 'Get day end.',


  inputs: {

    date: {
      type: 'number',
      required: false,
      default: null
    },

  },


  exits: {

    success: {
      description: '',
    },

  },


  fn: async function (inputs, exits) {
    let m = require('moment');
    let n = m(inputs.date).endOf('day').valueOf();

    return exits.success(n);
  }


};
