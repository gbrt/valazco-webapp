module.exports = {


  friendlyName: 'Get year start',


  description: 'Get year start.',


  inputs: {

    date: {
      type: 'number',
      required: false,
      default: null
    },

  },


  exits: {

    success: {
      description: '',
    },

  },


  fn: async function (inputs, exits) {
    let m = require('moment');
    let n = m(inputs.date).startOf('year').valueOf();

    return exits.success(n);
  }


};
