module.exports = {


  friendlyName: 'Get month start',


  description: 'Get month start.',


  inputs: {

    date: {
      type: 'number',
      required: false,
      default: null
    },

  },


  exits: {

    success: {
      description: '',
    },

  },


  fn: async function (inputs, exits) {
    let m = require('moment');
    let n = m(inputs.date).startOf('month').valueOf();

    return exits.success(n);
  }


};
