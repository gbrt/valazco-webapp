module.exports = {


  friendlyName: 'Get days duration',


  description: 'Get days duration.',


  inputs: {

    date: {
      type: 'number',
      required: false,
      default: null
    },

  },


  exits: {

    success: {
      description: '',
    },

  },


  fn: async function (inputs, exits) {
    let m = require('moment');
    let n = m.duration(inputs.date).asDays();

    return exits.success(n);
  }


};
