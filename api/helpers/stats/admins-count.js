module.exports = {


  friendlyName: 'Admins count',


  description: 'Admins count.',


  inputs: {

  },


  exits: {

    success: {
      description: 'Admins count',
    },

  },


  fn: async function (inputs, exits) {
    const adminRoles = [];
    
    Object.entries(sails.config.roles).forEach(([role, settings]) => {
      if (settings.admin) {
        adminRoles.push(role);
      }
    });
    
    const total = await User.count().where({'role': adminRoles});

    return exits.success(total);
  }


};
