module.exports = {

  friendlyName: 'Create Transaction',

  description: 'Create Transaction.',

  inputs: {
    userId: {
      description: 'The ID of the user to look up.',

      type: 'number',

      required: true
    },

    payeeId: {
      description: 'The ID of the user to look up.',

      type: 'number',

      required: true
    },

    amount: {
      description: 'The amount to add',

      type: 'number',

      required: true
    },

    reason: {
      description: 'Reason of the transaction',

      type: 'string',

      required: true
    },

    date: {
      type: 'number',

      required: false

    }
  },

  exits: {

    success: {
      description: 'The requesting socket is now subscribed to socket broadcasts about the logged-in user\'s session.',
    },

    insufficientFunds: {
      description: 'Insufficient funds',
    },

  },

  fn: async function (inputs, exits) {
    let vId = sails.config.user.valazcoUser;
    let transactionDate = inputs.date ? inputs.date : Date.now();
    let user = await User.findOne(inputs.userId);
    let payee = await User.findOne(inputs.payeeId);
    let amount = Math.trunc(inputs.amount * 100) / 100;
    let reason = inputs.reason || '---';

    if (user.id !== vId) {
      await sails.helpers.transaction.payout(user.id, amount)
        .intercept('insufficientFunds', 'insufficientFunds');
    }

    if (payee.id !== vId) {
      await sails.helpers.transaction.payin(payee.id, amount);
    }

    await Transaction.create({
      'amount': amount,
      'from': user.id,
      'to': payee.id,
      'reason': reason,
      'createdAt': transactionDate
    });

    if (user.id !== vId && payee.id !== vId) {
      await sails.helpers.user.updateStatusAfterTransaction(user.id, transactionDate);
      await sails.helpers.user.updateStatusAfterTransaction(payee.id, transactionDate);
    }

    return exits.success({});
  }
};
