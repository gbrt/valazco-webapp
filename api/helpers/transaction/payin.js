module.exports = {

  friendlyName: 'Add amount to funds',

  description: 'Add amount to funds.',

  inputs: {
    userId: {
      description: 'The ID of the user to look up.',

      type: 'number',

      required: true
    },
    amount: {
      description: 'The amount to add',

      type: 'number',

      required: true
    }
  },

  exits: {

    success: {
      description: 'The requesting socket is now subscribed to socket broadcasts about the logged-in user\'s session.',
    },

    zeroAmount: {
      description: 'amount = 0',
    },

    negativeAmount: {
      description: 'Negative amount',
    },

  },

  fn: async function (inputs, exits) {

    if(inputs.amount === 0) { throw 'zeroAmount'; }

    if(inputs.amount < 0) { throw 'negativeAmount'; }

    let user = await User.findOne(inputs.userId);

    if (!user) { throw 'notFound'; }
    
    let amount = Math.trunc(inputs.amount * 100) / 100;
    let currentFunds = user.funds;
    let newFunds = Math.round((currentFunds + amount) * 100) / 100;

    await User.updateOne(user.id).set({'funds': newFunds});

    return exits.success({});
  }
};
