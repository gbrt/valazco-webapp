module.exports = {


  friendlyName: 'Send verify email address',


  description: '',


  inputs: {
    
    userId: {
      type: 'number',
      required: true
    }
    
  },


  exits: {

    success: {
      description: 'All done.',
    },
    
    notFound: {
      description: 'User or unconfirmed email not found'
    }

  },


  fn: async function (inputs) {
    let user = await User.findOne(inputs.userId);
    
    if (!user) {
      throw 'notFound';
    }
  
    // Send "new account welcome" email
    await sails.helpers.email.sendTemplateEmail.with({
      from: sails.config.custom.internalEmailAddress,
      fromName: sails.config.custom.internalEmailAddress,
      to: user.email,
      subject: sails.__('mail.account.new.subject'),
      template: 'email-new-account',
      templateData: {
        fullName: user.fullName
      }
    });
  }


};

