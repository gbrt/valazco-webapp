module.exports = {


  friendlyName: 'Send verify email address',


  description: '',


  inputs: {
    
    userId: {
      type: 'number',
      required: true
    }
    
  },


  exits: {

    success: {
      description: 'All done.',
    },
    
    notFound: {
      description: 'User or unconfirmed email not found'
    }

  },


  fn: async function (inputs) {
    const user = await User.findOne(inputs.userId);
    
    if (!user) {
      throw 'notFound';
    }
    
    const recipients = await User.find().where({role: 'admin'});
    
    recipients.forEach(async (recipient) => {
      if (recipient.notifications.newUser) {
        await sails.helpers.email.sendTemplateEmail.with.with({
          from: sails.config.custom.internalEmailAddress,
          fromName: sails.config.custom.internalEmailAddress,
          to: recipient.email,
          subject: sails.__('mail.account.new.subject'),
          template: 'admin/email-new-account',
          templateData: {
            userId:   user.id,
            username: user.username,
            fullName: user.fullName,
            email:    user.email
          }
        });
      }
    });
  }


};

