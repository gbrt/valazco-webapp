module.exports = {


  friendlyName: 'Send verify email address',


  description: '',


  inputs: {
    
    userId: {
      type: 'number',
      required: true
    }
    
  },


  exits: {

    success: {
      description: 'All done.',
    },
    
    notFound: {
      description: 'User or unconfirmed email not found'
    }

  },


  fn: async function (inputs) {
    let user = await User.findOne(inputs.userId);
    
    if (!user || user.emailStatus === 'confirmed' || user.emailProofToken === '') {
      throw 'notFound';
    }
    
    if (sails.config.custom.verifyEmailAddresses) {
      // Send "confirm account" email
      await sails.helpers.email.sendTemplateEmail.with({
        from: sails.config.custom.internalEmailAddress,
        fromName: sails.config.custom.internalEmailAddress,
        to: user.email,
        subject: sails.__('mail.account.verify.subject'),
        template: 'email-verify-account',
        templateData: {
          fullName: user.fullName,
          token: user.emailProofToken
        }
      });
    } else {
      sails.log.info('Skipping new account email verification... (since `verifyEmailAddresses` is disabled)');
      await sails.helpers.email.sendNewAccountEmail(user.id);
    }
  }


};

