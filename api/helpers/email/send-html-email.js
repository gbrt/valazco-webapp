module.exports = {


  friendlyName: 'Send template email',


  description: 'Send an email using a template.',


  extendedDescription: 'To ease testing and development, if the provided "to" email address ends in "@example.com", '+
    'then the email message will be written to the terminal instead of actually being sent.'+
    '(Thanks [@simonratner](https://github.com/simonratner)!)',


  inputs: {

    to: {
      description: 'The email address of the primary recipient.',
      extendedDescription: 'If this is any address ending in "@example.com", then don\'t actually deliver the message. '+
        'Instead, just log it to the console.',
      example: 'nola.thacker@example.com',
      required: true,
      isEmail: true,
    },

    subject: {
      type: 'string',
      description: 'The subject of the email.',
      example: 'Hello there.',
      defaultsTo: ''
    },

    from: {
      type: 'string',
      description: 'An override for the default "from" email that\'s been configured.',
      example: 'anne.martin@example.com',
      isEmail: true,
    },

    text: {
      type: 'string',
      description: 'Text mail'
    },

    html: {
      type: 'string',
      description: 'HTML mail'
    }

  },


  fn: async function({to, from, subject, text, html}) {

    const { MailtrapClient } = require("mailtrap");
    const mailer = sails.config.custom.mailtrapTestInboxId ? 
                  new MailtrapClient({
                    token: sails.config.custom.mailtrapToken,
                    testInboxId: sails.config.custom.mailtrapTestInboxId,
                  }).testing :
                  new MailtrapClient({
                    token: sails.config.custom.mailtrapToken,
                  });
    
    const sender = {
      email: from,
      name: from,
    };

    const recipients = [
      {
        email: to,
      }
    ];
        
    mailer.send({
      from: sender,
      to: recipients,
      subject: subject,
      html: html,
    })
    .then(sails.log, sails.log);

    // All done!
    return {

    };

  }

};
