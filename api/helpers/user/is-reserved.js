module.exports = {

  friendlyName: 'Is reserved user?',

  description: 'Check if given user is reserved.',

  inputs: {

    username: {
      type: 'string',

      required: true
    },

  },

  exits: {

    success: {
      description: 'The requesting socket is now subscribed to socket broadcasts about the logged-in user\'s session.',
    },

  },

  fn: async function (inputs, exits) {
    let isReserved = false;

    if (sails.config.user.reservedUsernames.findIndex(u => {
      let re = new RegExp('.*' + u + '.*','g');
      if (re.test(inputs.username)) {
        return true;
      }
      else {
        return false;
      }
    }) > -1 ) {
      isReserved = true;
    }

    return exits.success(isReserved);
  }
};
