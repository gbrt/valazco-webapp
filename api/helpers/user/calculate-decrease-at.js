module.exports = {

  friendlyName: 'Return next decreaseAt',

  description: 'Return next decreaseAt.',

  inputs: {
    date: {
      type: 'number',

      required: true
    },
  },

  exits: {

    success: {
      description: 'The requesting socket is now subscribed to socket broadcasts about the logged-in user\'s session.',
    },

  },

  fn: async function (inputs, exits) {
    let date = inputs.date + sails.config.val.decreaseTime;

    return exits.success(date);
  }
};
